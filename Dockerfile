FROM maven:3-jdk-11 as builder
ARG REPOSITORY_USERNAME
ARG REPOSITORY_PASSWORD 
WORKDIR /buildfolder
COPY . .
RUN mvn -Drepository.username=$REPOSITORY_USERNAME -Drepository.password=$REPOSITORY_PASSWORD clean install package assembly:single -s environment/settings.xml

FROM de.icr.io/tegsoft/tegsofttouchbase:38-master-4bfca1c0-20210112165344

COPY --from=builder --chown=jboss:jboss /buildfolder/target/TegsoftTouchUserServices-jar-with-dependencies.jar /opt/jboss/wildfly/standalone/deployments/TegsoftTouch.war/WEB-INF/lib/