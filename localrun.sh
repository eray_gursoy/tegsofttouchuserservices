rm -rf localrun/TegsoftTouchUserServices-jar-with-dependencies.jar

mvn install package assembly:single

cp target/TegsoftTouchUserServices-jar-with-dependencies.jar localrun/

docker build -t tegsofttouchuserservices:latest . -f localrun/Dockerfile

docker run --env tobe_dburl="jdbc:db2://192.168.47.12:50000/tobe" --env UNITUID="4a55c1e3-edd5-46ef-b66f-d74634e8469a" --env tegsoft_touch_object_storage_apikey="1g_4a9DXwP0dAG-8ux3CgaFaPwhZTfefZXX6GZaYRjXL" --env tegsoft_touch_object_storage_service_instance_id="crn:v1:bluemix:public:cloud-object-storage:global:a/50d192df08a920eabc7d3577e1a5942d:1aeacaf0-3b15-417f-9ddb-14d0583defe6::" --env tegsoft_touch_object_storage_endpoint_url="https://control.cloud-object-storage.cloud.ibm.com/v2/endpoints" --env tegsoft_touch_object_storage_location="eu-de" --env tegsoft_touch_object_storage_companybucketname="tegsoft-touch-companylogos" --env tegsoft_touch_object_storage_profilebucketname="tegsoft-touch-profilepictures" -p 8082:8080 -v -t tegsofttouchuserservices:latest
