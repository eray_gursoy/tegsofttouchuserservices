package com.tegsoft.touch.components.ProfilePhoto;

import java.io.File;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.tegsoft.touch.util.ObjectStoreUtil;

public class ProfilePhotoRepository {

	private static final String LOCAL_IMAGE_DIRECTORY = System.getenv("tegsoft_touch_profilePhoto_imagePath");
	private static ObjectStoreUtil _objectStoreUtil = new ObjectStoreUtil();
	
	public void uploadImageToObjectStore(List<FileItem> items, String userId) throws Exception {
		for (FileItem item : items) {
			File writeFile = new File(LOCAL_IMAGE_DIRECTORY + userId);
			item.write(writeFile);
			if (_objectStoreUtil.createCompanyLogoObject(userId, writeFile)) {
				if (writeFile.exists()) {
					writeFile.delete();
				}
			}
		}
	}
	
	public void deleteImageFromObjectStorage(String filename) {
		_objectStoreUtil.deleteCompanyLogoItem(filename);
	}

	public File getImageFileFromObjectStorage(String userId) throws Exception {
		return _objectStoreUtil.getCompanyLogoObjectRequest(userId, LOCAL_IMAGE_DIRECTORY);
	}

}
