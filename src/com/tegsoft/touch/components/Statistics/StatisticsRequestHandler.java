package com.tegsoft.touch.components.Statistics;

import org.json.JSONObject;

import com.tegsoft.tobe.db.command.Command;
import com.tegsoft.tobe.db.dataset.DataRow;
import com.tegsoft.tobe.db.dataset.Dataset;
import com.tegsoft.touch.core.MicroServiceRequestHandler;
import com.tegsoft.touch.core.ServiceResponse;

public class StatisticsRequestHandler extends MicroServiceRequestHandler {

	public String getUserStatistics(String UID) {

		try {
			Dataset TBLCCCDR = new Dataset("TBLCCCDR", "TBLCCCDR");
			Command cmdTBLCCCDR = new Command("select * from TBLCCCDR where UID=");
			cmdTBLCCCDR.bind(UID);
			TBLCCCDR.fill(cmdTBLCCCDR);

			if (TBLCCCDR.getRowCount() == 0) {
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public ServiceResponse calculateMoneySaved(String UID) {
		int totalTalkTime = calculateTotalTalkTime(UID);
		int totalMoneySaved = 0;

		JSONObject moneySaved = new JSONObject();
		moneySaved.put("currency", "TL");
		if (totalTalkTime <= 0) {
			moneySaved.put("amount", 0);
			return getMicroServiceRequest().getResponseTemplates().success(moneySaved, "success");
		}

		int second = totalTalkTime % 60;
		int minutes = totalTalkTime / 60;

		if (second >= 0) {
			totalMoneySaved = (minutes + 1) * 7;
		}
		moneySaved.put("amount", totalMoneySaved);
		return getMicroServiceRequest().getResponseTemplates().success(moneySaved, "success");
	}

	public ServiceResponse calculateTimeSaved(String UID) {
		int totalTalkTime = calculateTotalTalkTime(UID);

		JSONObject timeSaved = new JSONObject();
		timeSaved.put("unit", "minutes");
		if (totalTalkTime <= 0) {
			timeSaved.put("count", 0);
			return getMicroServiceRequest().getResponseTemplates().success(timeSaved, "success");
		}

		int second = totalTalkTime % 60;
		int minutes = totalTalkTime / 60;

		String strSecond = String.valueOf(second);
		String strMinutes = String.valueOf(minutes);

		if (strSecond.length() == 1)
			strSecond = "0" + strSecond;
		if (strMinutes.length() == 1)
			strMinutes = "0" + strMinutes;

		String talktime = "";
		if (totalTalkTime < 60) {
			talktime = "00:" + strSecond;
		} else {
			talktime = strMinutes + ":" + strSecond;
		}

		timeSaved.put("count", talktime);
		return getMicroServiceRequest().getResponseTemplates().success(timeSaved, "success");
	}

	public ServiceResponse calculateScore(String usercode) {

		return new ServiceResponse();
	}

	public ServiceResponse getAllUserStatistics(String UID) {

		JSONObject result = new JSONObject();
		JSONObject timeValue = calculateTimeSaved(UID);
		JSONObject moneyValue = calculateMoneySaved(UID);
		result.put("saved_time", timeValue);
		result.put("saved_money", moneyValue);
		return getMicroServiceRequest().getResponseTemplates().success(result, "success");
	}

	private int calculateTotalTalkTime(String UID) {
		int totalTalkTime = 0;
		try {
			Dataset TBLCCCDR = new Dataset("TBLCCCDR", "TBLCCCDR");
			Command cmdTBLCCCDR = new Command("select DURATION from TBLCCCDR where UID=");
			cmdTBLCCCDR.bind(UID);
			TBLCCCDR.fill(cmdTBLCCCDR);

			if (TBLCCCDR.getRowCount() == 0) {
				return 0;
			}

			for (int i = 0; i < TBLCCCDR.getRowCount(); i++) {
				DataRow rowTBLCCCDR = TBLCCCDR.getRow(i);
				String strDuration = rowTBLCCCDR.getString("DURATION");
				int DURATION = Integer.parseInt(strDuration);
				totalTalkTime += DURATION;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return totalTalkTime;
	}
}
