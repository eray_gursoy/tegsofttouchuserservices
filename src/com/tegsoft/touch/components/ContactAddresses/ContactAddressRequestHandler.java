package com.tegsoft.touch.components.ContactAddresses;

import java.util.Map;
import com.tegsoft.tobe.db.Counter;
import com.tegsoft.tobe.db.command.Command;
import com.tegsoft.tobe.db.dataset.DataRow;
import com.tegsoft.tobe.db.dataset.Dataset;
import com.tegsoft.tobe.util.NullStatus;
import com.tegsoft.touch.core.MicroServiceRequestHandler;
import com.tegsoft.touch.core.ServiceResponse;

public class ContactAddressRequestHandler extends MicroServiceRequestHandler{

	public ServiceResponse getUserAddress(String UID) {

		try {
			Dataset TBLCRMCONTADDRESS = new Dataset("TBLCRMCONTADDRESS", "TBLCRMCONTADDRESS");
			DataRow rowTBLCRMCONTADDRESS;

			Command command = new Command("SELECT * FROM TBLCRMCONTADDRESS WHERE 1=1 ");
			command.append("AND UPPER(CONTID)=UPPER(");
			command.bind(UID);
			command.append(")");
			TBLCRMCONTADDRESS.fill(command);

			rowTBLCRMCONTADDRESS = TBLCRMCONTADDRESS.getRow(0);

			if (NullStatus.isNull(rowTBLCRMCONTADDRESS)) {
				return getMicroServiceRequest().getResponseTemplates().badRequest("Address not found");
			}

			return getMicroServiceRequest().getResponseTemplates().success(rowTBLCRMCONTADDRESS.toJSONString(),"Get User Successfull");
		} catch (Exception e) {
			e.printStackTrace();
			return getMicroServiceRequest().getResponseTemplates().internalError();
		}
	}

	public ServiceResponse createUserAddress(String UID, Map<String, String> addressCredentials) {

		try {
			Dataset TBLCRMCONTADDRESS = new Dataset("TBLCRMCONTADDRESS", "TBLCRMCONTADDRESS");
			DataRow rowTBLCRMCONTADDRESS = TBLCRMCONTADDRESS.addNewDataRow();

			if (NullStatus.isNull(rowTBLCRMCONTADDRESS)) {
				return getMicroServiceRequest().getResponseTemplates().internalError();
			}
			rowTBLCRMCONTADDRESS.set("CONTID", UID);
			rowTBLCRMCONTADDRESS.set("ADRESSID", Counter.getUUIDString());

			for (Map.Entry<String, String> entry : addressCredentials.entrySet()) {
				String key = entry.getKey().trim().toUpperCase();
				String value = entry.getValue().trim().toUpperCase();
				if (NullStatus.isNotNull(entry.getValue())) {
					rowTBLCRMCONTADDRESS.setString(key, value);
				}
			}
			TBLCRMCONTADDRESS.save();
		} catch (Exception e) {
			e.printStackTrace();
			return getMicroServiceRequest().getResponseTemplates().internalError();
		}
		return getMicroServiceRequest().getResponseTemplates().success("Creating new user successfull");
	}

	public ServiceResponse updateUserAddress(String UID, Map<String, String> addressCredentials) {
		
		try {
			Dataset TBLCRMCONTADDRESS = new Dataset("TBLCRMCONTADDRESS", "TBLCRMCONTADDRESS");
			DataRow rowTBLCRMCONTADDRESS;

			Command command = new Command("SELECT * FROM TBLCRMCONTADDRESS WHERE 1=1 ");
			command.append("AND UPPER(CONTID)=UPPER(");
			command.bind(UID);
			command.append(")");
			TBLCRMCONTADDRESS.fill(command);

			rowTBLCRMCONTADDRESS = TBLCRMCONTADDRESS.getRow(0);

			for (Map.Entry<String, String> entry : addressCredentials.entrySet()) {
				String key = entry.getKey().trim().toUpperCase();
				String value = entry.getValue().trim().toUpperCase();

				if (key.equalsIgnoreCase("ADRESSID") || key.equalsIgnoreCase("CONTID") || key.equalsIgnoreCase("UNITUID"))
					continue;

				if (NullStatus.isNotNull(entry.getValue())) {
					rowTBLCRMCONTADDRESS.setString(key, value);
				}
			}
			TBLCRMCONTADDRESS.save();
		} catch (Exception e) {
			e.printStackTrace();
			return getMicroServiceRequest().getResponseTemplates().internalError();
		}
		return getMicroServiceRequest().getResponseTemplates().success("Updating user address successfull");
	}

	public ServiceResponse deleteUserAddress(String UID) {

		String addressType = getParameter("addressType");
		if (NullStatus.isNull(addressType)) {
			return getMicroServiceRequest().getResponseTemplates().badRequest("addressType parameter is needed");
		}
		try {
			Command command = new Command("DELETE FROM TBLCRMCONTADDRESS WHERE 1=1 ");
			command.append("AND UPPER(CONTID)=UPPER(");
			command.bind(UID);
			command.append(")");
			command.append(" AND ADDRESSTYPE=");
			command.bind(addressType);

			command.executeNonQuery();
		} catch (Exception e) {
			e.printStackTrace();
			return getMicroServiceRequest().getResponseTemplates().internalError();
		}
		return getMicroServiceRequest().getResponseTemplates().success("Deleting user address successfull");
	}
}
