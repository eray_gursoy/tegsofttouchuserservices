package com.tegsoft.touch.components.ContactAddresses;

public class Address {

	private String city;
	private String country;
	private String postBox;
	private String zipCode;
	private String address;
	private String state;
	private String addressType;
	private String latitude;
	private String longitude;

	public Address() {
		super();
	}

	public Address(String city, String country, String postBox, String zipCode, String homeAddress, String state, String addressType, String latitude, String longitude) {
		super();
		this.city = city;
		this.country = country;
		this.postBox = postBox;
		this.zipCode = zipCode;
		this.address = homeAddress;
		this.state = state;
		this.addressType = addressType;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostBox() {
		return postBox;
	}

	public void setPostBox(String postBox) {
		this.postBox = postBox;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String homeAddress) {
		this.address = homeAddress;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Address {city:" + city + ", country:" + country + ", postBox:" + postBox + ", zipCode:" + zipCode + ", address:" + address + ", state:" + state + ", addressType:" + addressType + ", latitude:" + latitude + ", longitude:" + longitude + "]";
	}

}
