package com.tegsoft.touch.components.User;

import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONObject;

import com.tegsoft.tobe.db.command.Command;
import com.tegsoft.tobe.db.dataset.DataRow;
import com.tegsoft.tobe.db.dataset.Dataset;
import com.tegsoft.tobe.util.NullStatus;
import com.tegsoft.touch.core.MicroServiceRequestHandler;
import com.tegsoft.touch.core.ServiceResponse;
import com.tegsoft.touch.core.common.exception.NotFoundException;
import com.tegsoft.touch.core.common.exception.RequiredFieldException;

public class UserDetailRequestHandler extends MicroServiceRequestHandler {

	private static final Logger logger = Logger.getLogger(UserDetailRequestHandler.class.getCanonicalName());

	public Dataset list() throws Exception {

		Dataset TBLCRMCONTACTS = new Dataset("TBLCRMCONTACTS", "TBLCRMCONTACTS");
		Command command = new Command("SELECT * FROM TBLCRMCONTACTS");
		command.append("WHERE UNITUID = {UNITUID}");

		TBLCRMCONTACTS.fill(command);

		return TBLCRMCONTACTS;
	}

	public ServiceResponse getUser() throws RequiredFieldException, NotFoundException, Exception {
		String userId = getParameter("userId");
		if (NullStatus.isNull(userId)) {
			return getMicroServiceRequest().getResponseTemplates().badRequest("parameter userId is required");
		}

		Dataset TBLCRMCONTACTS = new Dataset("TBLCRMCONTACTS", "TBLCRMCONTACTS");
		Command command = new Command("SELECT * FROM TBLCRMCONTACTS");
		command.append("WHERE UNITUID = {UNITUID}");
		command.append("AND CONTID = ");
		command.bind(userId);

		TBLCRMCONTACTS.fill(command);

		if (TBLCRMCONTACTS.getRowCount() < 1) {
			logger.warning("Can not find any user with the specified userId");
			return getMicroServiceRequest().getResponseTemplates().notFound("User not found for userId :" + userId);
		}

		if (TBLCRMCONTACTS.getRowCount() > 1) {
			logger.warning("More than one record found for the given userId");
			return getMicroServiceRequest().getResponseTemplates().notFound("More than one record found for the given userId :" + userId);
		}

		JSONObject userDetail = new JSONObject();
		DataRow rowTBLCRMCONTACTS = TBLCRMCONTACTS.getRow(0);

		userDetail.put("user_id", rowTBLCRMCONTACTS.getString("CONTID"));
		userDetail.put("name", rowTBLCRMCONTACTS.getString("FIRSTNAME"));
		userDetail.put("surname", rowTBLCRMCONTACTS.getString("LASTNAME"));
		userDetail.put("gender", rowTBLCRMCONTACTS.getString("SEX"));
		userDetail.put("birthdate", rowTBLCRMCONTACTS.getString("BIRTHDAY"));
		userDetail.put("email", rowTBLCRMCONTACTS.getString("EMAIL1"));
		userDetail.put("phone", rowTBLCRMCONTACTS.getString("PHONE1"));

		return getMicroServiceRequest().getResponseTemplates().success(userDetail, "User has been found successfully");
	}

	public ServiceResponse update() throws IllegalArgumentException, RequiredFieldException, NotFoundException, Exception {
		Map<String, String> bodyContentMap = getMicroServiceRequest().getBodyContentMap();

		if (NullStatus.isNull(bodyContentMap)) {
			return getMicroServiceRequest().getResponseTemplates().badRequest("Unsupported Request : bodyContentMap is null");
		}

		String name = bodyContentMap.get("name");
		String surname = bodyContentMap.get("surname");
		String phone = bodyContentMap.get("phone");
		String gender = bodyContentMap.get("gender");

		if (NullStatus.isAnyNull(name, surname, phone, gender)) {
			logger.warning("update parameters are null");
			return getMicroServiceRequest().getResponseTemplates().badRequest("Unsupported Request : All fields to update are null");
		}

		String UID = getMicroServiceRequest().getUID();
		if (NullStatus.isNull(UID)) {
			logger.warning("userDetail object to update is null");
			throw new RequiredFieldException("userDetail object to update is null");
		}

		updateUser(UID, name, surname, phone, gender);
		updateContact(UID, name, surname, phone, gender);

		return getMicroServiceRequest().getResponseTemplates().success("User update successfull");
	}

	private void updateUser(String UID, String name, String surname, String phone, String gender) throws NotFoundException, Exception {
		try {
			Dataset dataset = new Dataset("TBLUSERS", "TBLUSERS");
			Command command = new Command("SELECT * FROM TBLUSERS");
			command.append("WHERE UNITUID = {UNITUID}");
			command.append("AND UID = ");
			command.bind(UID);

			dataset.fill(command);

			if (dataset.getRowCount() < 1) {
				logger.warning("Can not find any user with the specified userId");
			}

			if (dataset.getRowCount() > 1) {
				logger.warning("More than one user record found for the given userId");
			}

			DataRow row = dataset.getRow(0);

			if (NullStatus.isNotNull(name)) {
				row.set("USERNAME", name);
			}
			if (NullStatus.isNotNull(surname)) {
				row.set("SURNAME", surname);
			}

			row.update(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateContact(String UID, String name, String surname, String phone, String gender) throws Exception, NotFoundException {
		Dataset dataset = new Dataset("TBLCRMCONTACTS", "TBLCRMCONTACTS");
		Command command = new Command("SELECT * FROM TBLCRMCONTACTS");
		command.append("WHERE UNITUID = {UNITUID}");
		command.append("AND CONTID = ");
		command.bind(UID);

		dataset.fill(command);

		if (dataset.getRowCount() < 1) {
			logger.warning("Can not find any contact with the specified userId");
			throw new NotFoundException("Can not find any contact with the specified userId : " + UID);
		}

		if (dataset.getRowCount() > 1) {
			logger.warning("More than one contact record found for the given userId");
		}

		DataRow row = dataset.getRow(0);

		if (NullStatus.isNotNull(name)) {
			row.set("FIRSTNAME", name);
		}
		if (NullStatus.isNotNull(surname)) {
			row.set("LASTNAME", surname);
		}
		if (NullStatus.isNotNull(phone)) {
			row.set("PHONE1", phone);
		}
		if (NullStatus.isNotNull(gender)) {
			row.set("SEX", gender);
		}

		row.update(true);
	}

}
