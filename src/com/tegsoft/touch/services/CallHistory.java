package com.tegsoft.touch.services;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tegsoft.touch.core.Microservice;
import com.tegsoft.touch.core.ServiceResponse;

public class CallHistory extends Microservice {

	@Override
	public void init() throws Exception {
		authenticationRequired = true;
		allowedMethods = new RequestMethod[] { RequestMethod.GET };
	}

	@Override
	public ServiceResponse serviceCheck() throws Exception {
		return getMicroServiceRequest().getResponseTemplates().getStatusOkServiceResponse();
	}

	@Override
	public ServiceResponse performAction() throws Exception {

		try {
			if (getRequestMethod() == RequestMethod.GET)
				return getCallHistory();
		} catch (Exception e) {
			e.printStackTrace();
			return getMicroServiceRequest().getResponseTemplates().internalError();
		}

		return null;
	}

	private ServiceResponse getCallHistory() {
		JSONObject result = new JSONObject();
		
		JSONArray history = new JSONArray();
		
		for(int i=0; i<2; i++) {
			JSONObject item = new JSONObject();

			item.put("company_id", "12345");
			item.put("name", "Turkcell");
			item.put("img_path", USER_PHOTO_CDN_URL);
			item.put("call_count", 5);
			item.put("call_duration", 3);
			item.put("call_duration_time_unit", "minutes");
			
			history.put(item);
		}
		
		result.put("call_history", history);
		return getMicroServiceRequest().getResponseTemplates().success(result, "Call history have been listed successfully");
	}

	@Override
	public ServiceResponse displayHelp() throws Exception {
		return helpServiceResponse;
	}

}
