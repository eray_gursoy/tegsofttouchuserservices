//package com.tegsoft.touch.services;
//
//import com.tegsoft.tobe.util.NullStatus;
//import com.tegsoft.touch.components.UserStatistics.UserStatisticsRequestHandler;
//import com.tegsoft.touch.core.MicroServiceRequest;
//import com.tegsoft.touch.core.Microservice;
//import com.tegsoft.touch.core.ServiceResponse;
//
//public class UserStatistics extends Microservice {
//
//	@Override
//	public void init() throws Exception {
//		allowedMethods = new RequestMethod[] { RequestMethod.GET };
//		this.authenticationRequired=true;
//	}
//
//	@Override
//	public ServiceResponse serviceCheck() throws Exception {
//		return  MicroServiceRequest.getMicroServiceRequest().getStatusOkServiceResponse();
//	}
//
//	@Override
//	public ServiceResponse performAction() throws Exception {
//		
//		ServiceResponse statusErrorServiceResponse = getMicroServiceRequest().getStatusErrorServiceResponse();
//		ServiceResponse statusOkServiceResponse = getMicroServiceRequest().getStatusOkServiceResponse();
//		
//		String UID = getParameter("UID");
//		String strResponse ="";
//		
//		if (NullStatus.isNull(UID)) {
//			
//			statusErrorServiceResponse.put("errorcode", "PRM001");
//			statusErrorServiceResponse.put("errormessage", "usercode parameter is required");
//			return statusErrorServiceResponse;
//		}
//
//		String requestType = getParameter("requestType");
//		if (NullStatus.isNull(requestType)) {
//			requestType = "";
//		}
//		UserStatisticsRequestHandler userStatisticsRequestHandler = new UserStatisticsRequestHandler();
//		
//		switch (requestType) {
//		case "calculateMoneySaved":
//			strResponse=userStatisticsRequestHandler.calculateMoneySaved(UID);
//			break;
//		case "calculateTimeSaved":
//			strResponse=userStatisticsRequestHandler.calculateTimeSaved(UID);
//			break;
//		case "calculateScore":
//			strResponse=userStatisticsRequestHandler.calculateScore(UID);
//			break;
//		default:
//			strResponse=userStatisticsRequestHandler.getAllUserStatistics(UID);
//			break;
//		}
//
//		if(strResponse.startsWith("error") || strResponse.equals("error")) {
//			statusErrorServiceResponse.put("errorcode", "service_error_0001");
//			statusErrorServiceResponse.put("errormessage",strResponse);
//			return statusErrorServiceResponse;
//		}
//		
//		statusOkServiceResponse.put("response", strResponse);
//			
//		return statusOkServiceResponse;
//	}
//
//	@Override
//	public ServiceResponse displayHelp() throws Exception {
//		return null;
//	}
//
//}
