package com.tegsoft.touch.services;

import java.io.File;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.tegsoft.tobe.util.NullStatus;
import com.tegsoft.touch.components.ProfilePhoto.ProfilePhotoRepository;
import com.tegsoft.touch.core.Microservice;
import com.tegsoft.touch.core.ServiceResponse;

public class ProfilePhoto extends Microservice {

	private static ProfilePhotoRepository _photoRepository = new ProfilePhotoRepository();

	@Override
	public void init() throws Exception {
		allowedMethods = new RequestMethod[] { RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT };
		authenticationRequired = true;
	}

	@Override
	public ServiceResponse serviceCheck() throws Exception {
		return getMicroServiceRequest().getResponseTemplates().getStatusOkServiceResponse();
	}

	@Override
	public ServiceResponse performAction() throws Exception {
		String userId = getParameter("userId");
		RequestMethod requestMethod = getRequestMethod();
		if (NullStatus.isNull(userId)) {
			return getMicroServiceRequest().getResponseTemplates().badRequest("Unsupported Request : userId parameter is required for profile photo operations");
		}

		if (getRequestMethod() == RequestMethod.POST) {
			try {
				return createProfilePhoto(userId);
			} catch (Exception e) {
				e.printStackTrace();
				return getMicroServiceRequest().getResponseTemplates().internalError();
			}
		}

		if (requestMethod == RequestMethod.PUT) {
			try {
				return updateProfilePhoto(userId);
			} catch (Exception e) {
				e.printStackTrace();
				return getMicroServiceRequest().getResponseTemplates().internalError();
			}
		}

		if (requestMethod == RequestMethod.DELETE) {
			try {
				return deleteProfilePhoto(userId);
			} catch (Exception e) {
				e.printStackTrace();
				return getMicroServiceRequest().getResponseTemplates().internalError();
			}
		}

		return getMicroServiceRequest().getResponseTemplates().badRequest("Unsupported Request");

	}

	private ServiceResponse createProfilePhoto(String userId) throws Exception {
		List<FileItem> fileContentList =getMicroServiceRequest().getFileContentList();
		if (NullStatus.isNull(fileContentList)) {
			return getMicroServiceRequest().getResponseTemplates().badRequest("No file parameter found to upload");
		}

		_photoRepository.uploadImageToObjectStore(fileContentList, userId);

		return getMicroServiceRequest().getResponseTemplates().success(null, "Profile photo has been uploaded successfully");
	}

	private ServiceResponse updateProfilePhoto(String userId) throws Exception {
		List<FileItem> fileContentList =getMicroServiceRequest().getFileContentList();
		if (NullStatus.isNull(fileContentList)) {
			return getMicroServiceRequest().getResponseTemplates().badRequest("No file parameter found to upload");
		}

		File photoFile = _photoRepository.getImageFileFromObjectStorage(userId);
		if (NullStatus.isNotNull(photoFile)) {
			_photoRepository.deleteImageFromObjectStorage(userId);
		}

		_photoRepository.uploadImageToObjectStore(fileContentList, userId);

		return getMicroServiceRequest().getResponseTemplates().success(null, "Profile photo has been updated successfully");
	}

	private ServiceResponse deleteProfilePhoto(String userId) {
		_photoRepository.deleteImageFromObjectStorage(userId);

		return getMicroServiceRequest().getResponseTemplates().success(null, "Profile photo has been deleted successfully");
	}

	@Override
	public ServiceResponse displayHelp() throws Exception {
		return helpServiceResponse;
	}
}
