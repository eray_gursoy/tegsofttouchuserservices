package com.tegsoft.touch.services;

import java.util.Map;
import com.tegsoft.tobe.util.NullStatus;
import com.tegsoft.touch.components.ContactAddresses.ContactAddressRequestHandler;
import com.tegsoft.touch.core.Microservice;
import com.tegsoft.touch.core.ServiceResponse;

public class ContactAddresses extends Microservice {

	@Override
	public void init() throws Exception {
		allowedMethods = new RequestMethod[] { RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT };
		authenticationRequired = true;
	}

	@Override
	public ServiceResponse serviceCheck() throws Exception {
		return getMicroServiceRequest().getResponseTemplates().getStatusOkServiceResponse();
	}

	@Override
	public ServiceResponse performAction() throws Exception {

		String UID = getParameter("UID");
		if (NullStatus.isNull(UID)) {
			getMicroServiceRequest().getResponseTemplates().getStatusErrorServiceResponse().put("errormessage", "UID parameter cano not be null!");
		}

		Map<String, String> bodyContentMap = getMicroServiceRequest().getBodyContentMap();

		ContactAddressRequestHandler contactAddressRequestHandler = new ContactAddressRequestHandler();
		switch (getRequestMethod()) {
		case GET:
			return contactAddressRequestHandler.getUserAddress(UID);
		case POST:
			return contactAddressRequestHandler.createUserAddress(UID, bodyContentMap);
		case PUT:
			return contactAddressRequestHandler.updateUserAddress(UID, bodyContentMap);
		case DELETE:
			return contactAddressRequestHandler.deleteUserAddress(UID);
		default:
			break;
		}

		return null;
	}

	@Override
	public ServiceResponse displayHelp() throws Exception {
		return helpServiceResponse;
	}

}
