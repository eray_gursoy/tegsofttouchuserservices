package com.tegsoft.touch.services;

import com.tegsoft.touch.components.User.UserDetailRequestHandler;
import com.tegsoft.touch.core.Microservice;
import com.tegsoft.touch.core.ServiceResponse;

public class User extends Microservice {

	@Override
	public void init() throws Exception {
		authenticationRequired = true;
		allowedMethods = new RequestMethod[] { RequestMethod.GET, RequestMethod.PATCH };
	}

	@Override
	public ServiceResponse serviceCheck() throws Exception {
		return getMicroServiceRequest().getResponseTemplates().getStatusOkServiceResponse();
	}

	@Override
	public ServiceResponse performAction() throws Exception {
		UserDetailRequestHandler userDetailRequestHandler = new UserDetailRequestHandler();

		switch (getRequestMethod()) {
		case GET:
			return userDetailRequestHandler.getUser();
		case PATCH:
			return userDetailRequestHandler.update();
		default:
			break;
		}

		return null;
	}

//	private ServiceResponse getUserDetail(UserDetailRequestHandler userDetailRequestHandler) throws RequiredFieldException, NotFoundException, Exception {
//		String userId = getParameter("userId");
//		if (NullStatus.isNull(userId)) {
//			return badRequest("parameter userId is required");
//		}

//		Dataset TBLCRMCONTACTS = userDetailRequestHandler.get(userId);
//
//		if (NullStatus.isNull(TBLCRMCONTACTS)) {
//			return notFound("User not found for userId :" + userId);
//		}
//
//		if (TBLCRMCONTACTS.getRowCount() == 0) {
//			return notFound("User not found for userId :" + userId);
//		}

//		JSONObject userDetail = new JSONObject();
//		DataRow rowTBLCRMCONTACTS = TBLCRMCONTACTS.getRow(0);
//
//		userDetail.put("user_id", rowTBLCRMCONTACTS.getString("CONTID"));
//		userDetail.put("name", rowTBLCRMCONTACTS.getString("FIRSTNAME"));
//		userDetail.put("surname", rowTBLCRMCONTACTS.getString("LASTNAME"));
//		userDetail.put("gender", rowTBLCRMCONTACTS.getString("SEX"));
//		userDetail.put("birthdate", rowTBLCRMCONTACTS.getString("BIRTHDAY"));
//		userDetail.put("email", rowTBLCRMCONTACTS.getString("EMAIL1"));
//		userDetail.put("phone", rowTBLCRMCONTACTS.getString("PHONE1"));

//		return success(userDetail, "User has been found successfully");
//	}

//	private ServiceResponse performPATCHAction(UserDetailRequestHandler userDetailRequestHandler) throws IllegalArgumentException, RequiredFieldException, NotFoundException, Exception {
//		Map<String, String> bodyContentMap = getMicroServiceRequest().getBodyContentMap();
//
//		if (NullStatus.isNull(bodyContentMap)) {
//			return badRequest("Unsupported Request : bodyContentMap is null");
//		}

//		String name = bodyContentMap.get("name");
//		String surname = bodyContentMap.get("surname");
//		String phone = bodyContentMap.get("phone");
//		String gender = bodyContentMap.get("gender");

//		if (NullStatus.isAllNull(name, surname, phone, gender)) {
//			return badRequest("Unsupported Request : All fields to update are null");
//		}
//
//		String UID = getTouchUser().getUID();
//		if (NullStatus.isNull(UID)) {
//			return unauthorized("Can not get UID from token");
//		}
//
//		String params[] = new String[] { name, surname, phone, gender };
//
//		userDetailRequestHandler.update(UID, params);
//
//		return success(null, "User information have been updated successfully");
//	}

	@Override
	public ServiceResponse displayHelp() throws Exception {
		return helpServiceResponse;
	}

}
