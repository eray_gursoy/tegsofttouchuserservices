package com.tegsoft.touch.services;


import com.tegsoft.tobe.util.NullStatus;
import com.tegsoft.touch.components.Statistics.StatisticsRequestHandler;
import com.tegsoft.touch.core.Microservice;
import com.tegsoft.touch.core.ServiceResponse;

public class Statistics extends Microservice {

	@Override
	public void init() throws Exception {
		authenticationRequired = true;
		allowedMethods = new RequestMethod[] { RequestMethod.GET };
	}

	@Override
	public ServiceResponse serviceCheck() throws Exception {
		return getMicroServiceRequest().getResponseTemplates().getStatusOkServiceResponse();
	}

	@Override
	public ServiceResponse performAction() throws Exception {

		if (getRequestMethod() == RequestMethod.GET) {
			try {
				return performGETAction();
			} catch (Exception e) {
				e.printStackTrace();
				return getMicroServiceRequest().getResponseTemplates().internalError();
			}
		}

		return getMicroServiceRequest().getResponseTemplates().badRequest("Unsupported Request : Your request type is not supported");
	}

	private ServiceResponse performGETAction() {

		String UID = getParameter("UID");

		if (NullStatus.isNull(UID)) {
			return getMicroServiceRequest().getResponseTemplates().badRequest("usercode parameter is required");
		}

		String requestType = getParameter("requestType");
		if (NullStatus.isNull(requestType)) {
			requestType = "";
		}

		StatisticsRequestHandler userStatisticsRequestHandler = new StatisticsRequestHandler();

		switch (requestType) {
		case "calculateMoneySaved":
			return userStatisticsRequestHandler.calculateMoneySaved(UID);
		case "calculateTimeSaved":
			return userStatisticsRequestHandler.calculateTimeSaved(UID);
		case "calculateScore":
			return userStatisticsRequestHandler.calculateScore(UID);
		default:
			return userStatisticsRequestHandler.getAllUserStatistics(UID);
		}

	}

	@Override
	public ServiceResponse displayHelp() throws Exception {
		return helpServiceResponse;
	}

}
